/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/2/14
 * Time: 6:16 AM
 * To change this template use File | Settings | File Templates.
 */
/* The API controller
 Exports 3 methods:
 * post - Creates a new thread
 * list - Returns a list of threads
 * show - Displays a thread and its posts
 */

var util = require('../../util.js');

var logger = util.get_logger("api");


var shortid = require('shortid');
var mongo = require('../../db_connect/mongoose.js');
var csvWriter = require('csv-write-stream');
var fs = require('fs');
var crypto = require('crypto');
var guid = require('guid');
var _ = require('lodash-node');

var mongodb = require('mongodb');
var randomstring = require("randomstring");
var round = require('mongo-round');

var nodemailer = require('nodemailer');
var async = require('async');
var csvWriter = require('csv-write-stream')
var serverDB;
//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
var url = util.get_connection_string("mongo");

var keep_trail = function(business_id, business_id_name, collection_name, db_name, field_name, old_data, new_data, logger_msg) {
    var a = new audit();
    a.business_id = business_id;
    a.business_id_name = business_id_name;
    a.collection_name = collection_name;
    a.db_name = db_name;
    a.field_name = field_name;
    a.old_data = old_data;
    a.new_data = new_data;
    a.keep_trail();
    logger.info(logger_msg);
}

//********************  userinfo  methods   *************************************


//Email Sending for users --Suraksha
exports.emailsending_users = function(req, res) {

    var sub = "";
    if (req.body.program == 'Grassapparels') {

        sub = "Grass Email Submission";
    } else {
        sub = "Annectos Email Submission";
    }


    var curr_date = new Date();
    console.log(req.body)
    var query = {
        "user_name": req.body.name,
        "email_id": req.body.email,
        "mobile": req.body.phone,
        "program": req.body.program,
        "submission_dt": curr_date,
        "token": randomstring.generate(7) //added token field. 14Mar2017

    }

    console.log(url)
    url = "mongodb://emailUserRW:ou97cd@phoenix2mongo.goldencircle.in:42611/EmailUsers?readPreference=primary";
    MongoClient.connect(url, function(err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
        } else {
            var email = db.collection('user_email');
            email.insert(query, { w: 1 }, function(err, result) {
                if (err) {
                    logger.info("Error in Inserting user_info,err:" + err.message);

                } else {

                    var email_send = 1;
                    if (email_send == 1) {
                        var credited_datetime = new Date();
                        var point_credited_date = credited_datetime.getDate() + "/" + (credited_datetime.getMonth() + 1) + "/" + credited_datetime.getFullYear() + " " + credited_datetime.getHours() + ":" + credited_datetime.getMinutes() + ":" + credited_datetime.getSeconds();

                        // NB! No need to recreate the transporter object. You can use
                        // the same transporter object for all e-mails

                        // setup e-mail data with unicode symbols
                        var mailOptions = {
                            from: 'info@annectos.in', // sender address

                            to: 'sharvani@annectos.in,lalit@annectos.in',
                            // list of receivers
                            //to: 'suraksha@annectos.net',
                            //  cc: 'kuldip@annectos.in,lalit@annectos.in,sharvani@annectos.in,rahul.guha@gmail.com,suraksha@annectos.net,ankit@annectos.in',
                            // bcc: 'hassan@annectos.in,ista@annectos.in',
                            subject: sub, // Subject line
                            text: 'You have recieved new enquiry from: "' + req.body.name + '"; Mobile Number: ' + req.body.phone + '; Email Id: ' + req.body.email // plaintext body
                        };

                        // create reusable transporter object using SMTP transport
                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'info@annectos.in',
                                pass: 'phoenixII'
                            }
                        });

                        //                                                  send mail with defined transport object

                        transporter.sendMail(mailOptions, function(error, info) {
                            if (error) {
                                return logger.info("Error Occured In Email Sending : " + error);
                            }
                            logger.info('Email Sent: ' + info.response);
                        });

                    }

                }
            });




            util.send_to_response({ "msg": "Mail Sent" }, res);

        }
    });
}



//Email Sending for users --Suraksha

//updating date and time users who download the case study pdf --Suraksha 14Mar2017

exports.downloading_users = function(req, res) {




        //Initialize Distributor Collection
        var curr_date = new Date();
        console.log(req.query)


        MongoClient.connect(url, function(err, db) {
            if (err) {
                logger.info("Unable to connect to the mongoDB server. Error:" + err);
            } else {
                var user_details = db.collection('user_email');
                var d_time = new Date();
                var search = req.query.credential
                var query = { "token": search };
                var data = { '$push': { "download_time": new Date(d_time) } };
                console.log(query);

                user_details.update(query, data, { w: 1 }, function(err, result) {
                    if (err) {
                        logger.info("Error in Updating  Data,err:" + err.message);
                        util.send_to_response({ msg: 'Error in Data', err: err }, res);
                        //Close connection
                        db.close();
                    } else {
                        logger.info(" Updated Successfully");
                        //Close connection
                        db.close();
                        util.send_to_response({ msg: ' Updated Successfully' }, res);
                    }

                });

            }
        });
    }
    //updating date and time users who download the case study pdf --Suraksha 14Mar2017




//Fetching user email,token and generating link for each user --Suraksha 14Mar2017
exports.get_users_csv = function(req, res) {



    var curr_date = new Date();
    console.log(req.query)


    MongoClient.connect(url, function(err, db) {
        if (err) {
            logger.info("Unable to connect to the mongoDB server. Error:" + err);
        } else {
            var user_details = db.collection('user_email');

            user_details.find({}, { email_id: 1, token: 1, _id: 0 }).toArray(function(err, result) {
                if (err) {
                    logger.info("Error in   Data,err:" + err.message);
                    util.send_to_response({ msg: 'Error in Data', err: err }, res);
                    //Close connection
                    db.close();
                } else {
                    logger.info("Fetched data Successfully");
                    //Close connection
                    db.close();
                    var Execute_Time = new Date();
                    var file_name = "users" + Execute_Time.getFullYear() + "_" + (Execute_Time.getMonth() + 1) + "_" + Execute_Time.getDate() + "_" + Execute_Time.getHours() + "_" + Execute_Time.getMinutes() + "_" + Execute_Time.getSeconds();;
                    var writer = csvWriter()
                        //    writer.pipe(fs.createWriteStream('out.csv'))
                    writer.pipe(fs.createWriteStream('CSVFile/' + file_name + '.csv'));
                    var link = "";
                    for (var i = 0; i < result.length; i++) {

                        link = "http://annectosworld.com/download/index.html_rex?token=" + result[i].token
                        result[i].link = link;
                        writer.write(result[i]);
                    }
                    writer.end()
                    console.log("Success");
                }

            });

        }
    });
}

//Fetching user email,token and generating link for each user --Suraksha 14Mar2017




/*
 exports.generate_token_users = function (req, res) {




 //Initialize Distributor Collection
 var curr_date = new Date();
 console.log(req.query)


 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 } else {
 var user_details = db.collection('user_email');

 var search = {"token" :{$exists:false}}
 user_details.find(search).toArray(function (err, result) {
 if (err) {
 logger.info("Error in Updating Todo Data,err:" + err.message);
 util.send_to_response({msg: 'Error in Data', err: err}, res);
 //Close connection
 db.close();
 } else {
 console.log(result.length);
 //Close connection
 //db.close();
 var obj;var arr=[];
 for(var i=0;i<result.length;i++)
 {
 obj={"email_id" : result[i].email_id,
 "token" : randomstring.generate(7)}
 arr.push(obj)
 }


 console.log(arr)

 async.each(arr,function(value,callback)
 {
 if(value != "") {
 clubbing(value)
 }
 callback();

 },function(err)
 {
 if(err)
 {
 console.log("sucess nul")
 }
 else
 {
 console.log("sucess")

 util.send_to_response({"msg":"Success"},res);
 }
 })

 //     console.log( arr);

 }

 });

 }
 });
 }


 function clubbing(value1)
 {
 MongoClient.connect(url, function (err, db) {
 if (err) {
 logger.info("Unable to connect to the mongoDB server. Error:" + err);
 } else {
 var user_details = db.collection('user_email');
 var query = {"email_id" : value1.email_id};
 var data = {"$set":{token : value1.token}};
 user_details.update(query, data, {multi:true}, function (err, result) {
 if (err) {
 logger.info("Error in Updating Todo Data,err:" + err.message);
 util.send_to_response({msg: 'Error in Data', err: err}, res);
 //Close connection
 db.close();
 } else {
 logger.info("Todo Updated Successfully");
 //Close connection
 // db.close();
 //   util.send_to_response({msg: 'Updated Successfully'}, res);
 }

 });

 }
 });


 }*/



//updating date and time users who download the case study pdf for rlaxo --Suraksha 22Mar2017

exports.downloading_users_rex = function(req, res) {




        //Initialize Distributor Collection
        var curr_date = new Date();
        console.log(req.query)


        MongoClient.connect(url, function(err, db) {
            if (err) {
                logger.info("Unable to connect to the mongoDB server. Error:" + err);
            } else {
                var user_details = db.collection('user_email');
                var d_time = new Date();
                var search = req.query.credential
                var query = { "token": search };
                var data = { '$push': { "download_time_rex": new Date(d_time) } };
                console.log(query);

                user_details.update(query, data, { w: 1 }, function(err, result) {
                    if (err) {
                        logger.info("Error in Updating  Data,err:" + err.message);
                        util.send_to_response({ msg: 'Error in Data', err: err }, res);
                        //Close connection
                        db.close();
                    } else {
                        logger.info(" Updated Successfully");
                        //Close connection
                        db.close();
                        util.send_to_response({ msg: 'Updated Successfully' }, res);
                    }

                });

            }
        });
    }
    //updating date and time users who download the case study pdf for relaxo --Suraksha 22Mar2017