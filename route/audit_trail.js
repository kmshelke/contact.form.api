/**
 * Created by rahulguha on 9/20/14.
 */

// var rest =       require('restler'),
var    util =          require('../util.js');
var logger =        util.get_logger("audit");
logger.info("audit trail started");
//audit object
function audit () {
    "use strict";
    this.audit_id = "";
    this.app_id = util.get_audit_trail_config().app_id;
    this.secret = util.get_audit_trail_config().secret;
    this.db_name = "";
    this.collection_name = "";
    this.field_name = "";
    this.business_id ="";
    this.old_data = "";
    this.new_data  = "";
    this.ts = new Date();
    this.user = "";


    this.keep_trail = function(){
        try {
            var http = require('http');
            console.log(this);

            var logInfoString = JSON.stringify(this);

            var headers = {
                'Content-Type': 'application/json',
                'Content-Length': logInfoString.length
            };

            var options = {
                host: util.get_audit_trail_config().audit_trail_host,
                port: util.get_audit_trail_config().audit_trail_port,
                path: util.get_audit_trail_config().audit_trail_path,
                method: 'POST',
                headers: headers
            };

            var req = http.request(options, function(res) {
                res.setEncoding('utf-8');

                var responseString = '';

                res.on('data', function(data) {
                    responseString += data;
                });

                res.on('end', function() {
                    var resultObject = JSON.parse(responseString);
                    logger.info("audit trail -  captured - " + responseString);
                });
            });

            req.on('error', function(err) {
                logger.info("audit trail - system error -" + JSON.stringify(err));
            });

            req.write(logInfoString);
            req.end();

            // call audit trail endpoint in unblocked manner
           /* rest.postJson( util.get_audit_trail_config().endpoint, this)
                    .on('complete', function(data, response) {
                        logger.info("audit trail -  captured - " + response.rawEncoded);
                    })
                    .on('fail', function(data, response) {
                            logger.info("audit trail - end point not found -" + response.rawEncoded);
                        })
                    .on('error', function(err, response) {
                            logger.info("audit trail - system error -" + JSON.stringify(err));
                    });*/
        }
        catch (e) {
            console.log(e);
        }
    }

};

module.exports = audit;
