//<editor-fold desc="Variable Declaration">

var error_handler = require('./error').errorHandler,
    master_handler = require('./master'),
    express = require('express'),
    util = require('../util.js'),
    repo = require('../repo')
    ;


var logger = util.get_logger("server");
// define routers (new in Express 4.0)
var ping_route = express.Router(),
    master_route = express.Router(),
    help_route = express.Router(),
    login_route = express.Router()
    ;


//</editor-fold>


module.exports = exports = function (app, corsOptions) {
    //<editor-fold desc="Fast Phase">
    // protect any route starting with "master" with validation
    app.all("/master/*", authenticate, function (req, res, next) {
        next(); // if the middleware allowed us to get here,
        // just move on to the next route handler
    });

    // implement ping_route actions
    // This is specific way to inject code when this route is called
    // This executes for any ping route.
    // Similarly we can (and should) implement same for every route
    ping_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "ping", "PING"));
        // continue doing what we were doing and go to the route
        next();
    });
    ping_route.get('/', function (req, res) {
        res.send({'error': 0, 'msg': 'audit endpoints ready for data capture'});
    });
    ping_route.get('/check/mongo', function (req, res) {
        res.send({'error': 0, 'msg': 'Code for checking Mongo connection will be implemented here'});
    });
    ping_route.post('/check/post', function (req, res) {
        res.send('Checking Post Method' + req.body);
    });
    app.use('/ping', ping_route);
    // end ping route
    login_route.post('/user/detial', function (req, res) {
        api_master.emailsending_users(req, res);
    });


    login_route.get('/user/download', function (req, res) {
        api_master.downloading_users(req, res);
    });

    login_route.get('/user/token', function (req, res) {
        api_master.generate_token_users(req, res);
    });
    login_route.get('/user/csv', function (req, res) {
        api_master.get_users_csv(req, res);
    });


    login_route.get('/user/download/rex', function (req, res) {
        api_master.downloading_users_rex(req, res);
    });
    // master route implementation
    master_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "master", "MASTER"));
        // continue doing what we were doing and go to the route
        next();
    });
    //</editor-fold>

    //<editor-fold>
    var api_master = master_handler.api_master;
//    master_route.post('/renew/token', function(req, res){
//        api_master.renew_token(req,res);
//    });
    //</editor-fold>


    login_route.post('/user/detial', function (req, res) {
        api_master.emailsending_users(req, res);
    });

    // End




    // implement help_route actions
    help_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "help", "HELP"));
        // continue doing what we were doing and go to the route
        next();
    });
    help_route.get('/', function (req, res) {
        var r_list = [];
        get_routes(ping_route.stack, r_list, "ping");
        get_routes(master_route.stack, r_list, "master");
        get_routes(help_route.stack, r_list, "help");
        res.send(r_list);
    });
    help_route.get('/show', function (req, res) {
        var r_list = [];
        get_routes(ping_route.stack, r_list, "ping");
        get_routes(master_route.stack, r_list, "master");
        get_routes(help_route.stack, r_list, "help");
        //res.send(r_list);
        console.log(r_list);
        res.render('show', {'routes': r_list});
    });
    app.use('/help', help_route);
    // end help route

    //implement login_route action
    login_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "login", "LOGIN"));
        // continue doing what we were doing and go to the route
        next();
    });

//    For forgot password added by debolina

    login_route.post('/user/details', function (req, res) {
        api_master.get_user_id_password(req, res);
    });
    login_route.post('/insert/user_pass', function (req, res) {
        api_master.insert_user_pass(req, res);
    });
    login_route.post('/find/valid/user_token', function (req, res) {
        api_master.valid_user_token(req, res);
    });
    login_route.post('/user/update', function (req, res) {
        api_master.user_update(req, res);
    });

    login_route.post('/user/token/match', function (req, res) {
        api_master.token_match(req, res);
    });


    //Added by Mahiruddin on date 29/10/2015 for slap_reports
    master_route.get('/retailer/reports/slab', function (req, res) {
        api_master.slab_reports(req, res);
    });

    //Addition End

    //Added by Mahiruddin on date 26/07/2016 for Retailer Gifts
    master_route.post('/retailer/gift/list', function (req, res) {
        api_master.retailer_gift_list_by_code(req, res);
    });

    //empty or blank retailer gift array from retailer table after order place --> Added by Mahiruddin on date 26/07/2016
    //login_route.post('/retailer/gift/list/empty', function (req, res) {
    //    api_master.blank_retailer_gift_list_by_code(req, res);
    //});

    //master_route.get('/retailer/coupon/count', function (req, res) {
    //    api_master.retailer_coupon_count(req, res);
    //});


    //Addition End


    //default assign to the new telecaller

    master_route.post('/userinfo/add/defaultassign', function (req, res) {
        api_master.add_default_assign(req, res);
    });


    //***********Distributor Reversal Process added Start, by Mahiruddin Seikh : Date - 28/07/2016**********//

    master_route.post('/distributor/allocation/data/by/process/id', function (req, res) {
        api_master.get_allcation_data_by_process_id(req, res);
    });

    master_route.post('/distributor/transaction/data/by/process/id', function (req, res) {
        api_master.get_tran_data_by_process_id(req, res);
    });

    master_route.post('/disti/retailer/reverse', function (req, res) {
        api_master.disti_retailer_reverse(req, res);
    });
    //***********Distributor Reversal Process end, added by Mahiruddin Seikh**********//

    //**********************Admin User Login Via Login Route Start************************************//

    login_route.post('/by', function (req, res) {
        api_master.user_login(req, res);
    });
    login_route.post('/get/retailer/by/mobile', function (req, res) {
        api_master.get_retailer_data_by_reg_mob_no(req, res);
    });
    //<editor-fold desc="renew token 20141112 srijan">
    master_route.post('/renew/token', function (req, res) {
        api_master.renew_token(req, res);
    });

    //**********************Admin User Login Via Login Route Start************************************//

    //**********************Admin Coupon Creation Via Master Route Start**********************************//
    login_route.post('/generate/coupon', function (req, res) {
        api_master.generate_coupon(req, res);
    });
    //**********************Admin Coupon Creation Via Master Route End************************************//

    app.use('/login', login_route);
    app.use('/help', help_route);
    app.use('/master', master_route);

    //end login_route

    // This will be refactored later for adding more features and making it more generic
    var get_routes = function (r, r_list, route_sub_system) {
        for (var i = 0; i < r.length; i++) {
            if (typeof r[i].route != "undefined") {

                r_list.push(
                    {
                        'path': "/" + route_sub_system + r[i].route.path,
                        'method': r[i].route.methods
                    }
                )
            }
        }
        return r_list;
    }

    // and it will validate based on registered data
    function authenticate(req, res, next) {
        var token_data = token.verify(req.headers.token);
        if (token_data) {
            next();
        } else {

            res.send('Not authorised');
        }
    }

    // Error handling middleware
    app.use(error_handler);

    //</editor-fold>


}