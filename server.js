/**
 * Created by rahulguha on 5/3/15.
 */

var express = require('express'),
    bodyParser = require('body-parser'),
    route = require('./route'),
    config = require('./config/config.json'),
    util = require('./util.js'),
    _ = require('lodash-node'),
    cors = require('cors'),
    exphbs = require('express-handlebars');
;


// start logger
var logger = util.get_logger("server");

// start app
var app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());        // for x-browser
logger.info("express loaded");

// define view engine for help
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');


route(app);

process.on('uncaughtException', function (err) {
    setTimeout(function () {
        console.log("Uncaught error happened");
        logger.info(util.create_routing_log("uncaught error", "uncaught error", JSON.stringify(err), "uncaught error- exitting"));
    }, 2000);
    console.log("Uncaught exception!", err);
});

logger.info("all routes are loaded");
var server = app.listen(util.get_listening_port());
logger.info("http server started");
logger.info('Listening to port ' + util.get_listening_port());

var io = require("socket.io").listen(server, {log: false, origins: '*:*'});
io.on("connection", function (socket) {
    // to make things interesting, have it send every second
    socket.on("tele_action_wp_in", function (todo_id) {
        socket.broadcast.emit("tele_action_wp_out", todo_id);
    });
    socket.on("tele_action_wp_cancel_in", function (todo_id) {
        socket.broadcast.emit("tele_action_wp_cancel_out", todo_id);
    });
    socket.on("disconnect", function () {
        //clearInterval(interval);
    });
});


module.exports = app;