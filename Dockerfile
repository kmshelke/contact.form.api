FROM	node

# Bundle app source
COPY . /src
WORKDIR /src
# Install app dependencies
RUN npm install
EXPOSE  3000
CMD ["node", "server.js"]