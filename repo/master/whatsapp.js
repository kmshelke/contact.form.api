/**
 * Created by dev11 on 11/17/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');



var whatsappSchema = new Schema({
    "w_id" : { type: Number, trim: true,required:true,unique:true },
    "key_remote_jid" : { type: String, trim: true },
    "key_from_me" : { type: String, trim: true},
    "key_id" : { type: String, trim: true },
    "status" : { type: String, trim: true},
    "needs_push" : { type: String, trim: true},
    "timestamp" : { type: Date},
    "mobile_number":{type: String, trim: true},
    "coupons_data": [
        {
            coupon_code            : { type: String,  trim: true },
            coupons_status          : { type: String,  trim: true}
        }
    ],
    "coupon_status":{type: String, trim: true},
    "media_url" : { type: String, trim: true },
    "media_mime_type" : { type: String, trim: true },
    "media_wa_type" : { type: String, trim: true},
    "media_size" : { type: String, trim: true},
    "media_name" : { type: String, trim: true },
    "media_caption" : { type: String, trim: true },
    "media_hash" : { type: String, trim: true },
    "media_duration" : { type: String, trim: true},
    "origin" : { type: String, trim: true},
    "latitude" : { type: Number, trim: true},
    "longitude" : { type: Number, trim: true},
    "remote_resource" : { type: String, trim: true },
    "received_timestamp" : { type: Date},
    "send_timestamp" : { type: Date},
    "receipt_server_timestamp" : { type: Date },
    "receipt_device_timestamp" : { type: Date},
    "read_device_timestamp" : { type: Date},
    "played_device_timestamp" : { type: Date},
    "recipient_count" : { type: String, trim: true},
    "participant_hash" : { type: String, trim: true },
    "modified_dt" :{type: Date}
},{ versionKey: false });

var include_field="timestamp media_url mobile_number coupons_data coupon_status w_id -_id";
var include_field1="mobile_number";
var include_fields = "w_id _id";

whatsappSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}

whatsappSchema.statics.get_filter_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}

whatsappSchema.statics.get_mobile_number_list = function (skip,limit) {
    return this.find()
        .select(include_field1)
        .sort()
        .exec();
}


whatsappSchema.statics.get_coupon_count = function (query) {
    return this.find(query).count().exec();// Should return a Promise
}

whatsappSchema.statics.get_whatsapp_id = function(){
    return this.find()
        .select(include_fields)
        .sort({w_id :-1})
        .exec(); //Should return a Promise

}

whatsappSchema.statics.update_whatsapp_data = function(data){

    var q ={"w_id":data.w_id };

    return this.update
    (
        q,
        {
            "coupons_data":data.coupon_data,
            "coupon_status":"Coupon Submitted",
            "modified_dt":new Date()
        }
    ).exec();
}



whatsappSchema.set('collection', 'whatsapp');
module.exports = mongo.get_mongoose_connection().model('whatsapp', whatsappSchema);
