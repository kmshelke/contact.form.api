/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var userinfoSchema = new Schema({
    user_id          :  { type: String,  trim: true ,required: true, unique: true},
    user_type        : { type: String,  trim: true },
    user_region        : { type: String,  trim: true },
    user_group_id    : { type: Number},
    user_first_name  : { type: String,  trim: true },
    user_middle_name : { type: String,  trim: true },
    user_last_name   : { type: String,  trim: true },
    user_designation : { type: String,  trim: true },
    user_password    : { type: String,  trim: true },
    user_registed_no : { type: String,  trim: true },
    user_email_id    : { type: String,  trim: true },
    status           : { type: Number},
    created_by       : { type: String,  trim: true },
    created_dt       : { type: Date},
    modified_by      : { type: String,  trim: true },
    modified_dt      : { type: Date}
},{ versionKey: false });

var include_field = "user_id user_type user_first_name user_middle_name user_last_name user_designation user_password user_email_id user_group_id user_registed_no user_region -_id";
var include_field1="user_id user_email_id -_id user_first_name user_last_name";

userinfoSchema.statics.get_user_detail = function (query) {
    return this.find(query).select(include_field).exec();// Should return a Promise
}


userinfoSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}


userinfoSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}

userinfoSchema.statics.get_user_by_type = function (query) {
    return this.find(query).select("user_id user_first_name user_middle_name user_last_name -_id").exec();// Should return a Promise
}


userinfoSchema.statics.get_user_id_password = function (query) {
    return this.find(query).select(include_field1).exec();// Should return a Promise
}

userinfoSchema.set('collection', 'userinfo');
module.exports = mongo.get_mongoose_connection().model('userinfo', userinfoSchema);