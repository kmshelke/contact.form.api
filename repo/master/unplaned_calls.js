/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');
var autoIncrement = require("mongodb-autoincrement");

var unplaned_callsSchema = new Schema({
    uid                   : { type: String, trim: true ,required: true, unique: true},
    retailer_code                   : { type: String, trim: true ,required: true},
    retailer_extrainfo              : { },
    calldate                        : { type: Date },
    purpose                         : { type: String,  trim: true },
    telecaller                      : { },
    created_by                         : { type: String,  trim: true }
},{ versionKey: false }
 );


//Get Retailer List By State
unplaned_callsSchema.statics.get_retailer_by_state = function (query) {
    return this.find(query).select("retailer_code retailer_name -_id").exec();// Should return a Promise
}

//Update Retailer Detail
unplaned_callsSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}


unplaned_callsSchema.set('collection', 'unplaned_calls');
module.exports = mongo.get_mongoose_connection().model('unplaned_calls', unplaned_callsSchema);