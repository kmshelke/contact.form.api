/**
 * Created by Hassan Ahamed on 6/23/2015.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var todoSchema = new Schema({
    todo_id: {type: String, trim: true},
    wip_ts: {type: Date},
    wip_user: {},
    start_ts: {type: Date},
    stop_ts: {type: Date},
    "region": {type: String, trim: true},
    "state": {type: String, trim: true},
    "district": {type: String, trim: true},
    "city": {type: String, trim: true},
    area: {type: String, trim: true},
    retailer_status: {type: String, trim: true},
    old_record: {type: String, trim: true},
    call_comment: {type: String, trim: true},
    call_status: {type: String, trim: true},
    last_call_comment: {type: String, trim: true},
    status: {type: String, trim: true},
    call_hold: {type: String, trim: true},
    extra_info: {}
});

var include_fields = "type id todo_id todo_for created_by region state district extra_info last_call_comment coupon_code retailer_status reason -_id created_dt";

var include_fields1 = "type status id todo_for created_by todo_for last_call_comment region state district city locality retailer_status extra_info todo_id created_dt active assigned modified_by modified_dt area call_comment call_status medium -_id";

// static methods


//Get all Default Roster
todoSchema.statics.list_search_by_role = function (role) {
    var pipeline = [
        {
            $match: {
                $and: [
                    role,
                    {status: "Pending"},
                    {wip_ts: {$not: {$gt: new Date()}}},
                    {old_record: "0"},
                    {active: 1}

                ]
            }
        },
        {
            "$group": {
                "_id": {
                    "type": "$type",
                    "region": "$region",
                    "state": "$state",
                    "district": "$district",
                    "city": "$city",
                    "area": "$area",
                    "locality": "$locality"
                },
                count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                "type": "$_id.type",
                "region": "$_id.region",
                "state": "$_id.state",
                "district": "$_id.district",
                "city": "$_id.city",
                "area": "$_id.area",
                "locality": "$_id.locality",
                "count": "$count"
            }
        }

    ];
    return this.aggregate(pipeline).exec(); // Should return a Promise
}


todoSchema.statics.search_by_role = function (role, user_id, skip, limit, filterby, type) {

    var query={};

    //    var x= []
    //    if(area!=""){ x.push({area:area}); }
    //    if(locality!=""){ x.push({locality:locality}); }
    //    x.push({old_record:"0"});
    //    x.push({call_hold:user_id});
    //    x.push({status: "Pending"});
    //    x.push({wip_ts:{ $not:{ $gt:new Date()}}});
    //    if(role!=""){ x.push({type: role}); }
    //    if(region!=""){
    //        x.push({region: region});
    //    }
    //    var query={$and:x}

    //    console.log('Hassan',role)

    if (filterby && filterby != "") {

        query = {
            $and: [
                role,
                {
                    $or: [
                        {"type": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"status": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"id": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"todo_for": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"retailer_status": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"region": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"state": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"district": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"city": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"area": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"locality": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"landmark": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"todo_id": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"created_by": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"call_comment": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"extra_info.retailer_name": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"extra_info.outlet_name": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"extra_info.retailer_registered_mobile": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"extra_info.address": new RegExp("^.*?" + filterby + ".*?", "i")},
                        {"extra_info.pin": new RegExp("^.*?" + filterby + ".*?", "i")}
                    ]
                },
                {status: "Pending"},
                {wip_ts: {$not: {$gt: new Date()}}},
                {old_record: "0"},
                {call_hold: user_id}

            ]
        }

    }
    else {
        query = {
            $and: [
                role,
                {status: "Pending"},
                {wip_ts: {$not: {$gt: new Date()}}},
                {old_record: "0"},
                {call_hold: user_id}
            ]
        }
    }

    //console.log(query);

    //var query = {
    //    $and: [
    //        role,
    //        {status: "Pending"},
    //        {wip_ts: {$not: {$gt: new Date()}}},
    //        {old_record: "0"},
    //        {call_hold: user_id}
    //
    //    ]
    //}
    return this.find(query).skip(skip).limit(limit).select(include_fields).sort({created_dt: -1}).exec();

}

todoSchema.statics.get_todo_details_by_todo_id = function (x) {
    var query = {};
    if (x.todo_id) {
        query.todo_id = x.todo_id;
    }
    if (x.id) {
        query.id = x.id;
    }

    return this.find(query).select(include_fields1).exec();
}

todoSchema.statics.todo_update_for_wip = function (todo_id, wip_user) {
    var d = new Date();
    var e = new Date();
    d.setHours(d.getHours() + 1);
    return this.update({todo_id: todo_id}, {
        $set: {
            wip_ts: d,
            wip_user: wip_user,
            start_ts: e
        }
    }).exec(); // Should return a Promise
}

todoSchema.statics.todo_update_stop_time = function (todo_id) {
    var d = new Date();
    return this.update({todo_id: todo_id}, {$set: {stop_ts: d}}).exec(); // Should return a Promise
}

todoSchema.statics.todo_update_old_record = function (todo_id, val) {
    return this.update({todo_id: todo_id}, {$set: {old_record: val}}).exec(); // Should return a Promise
}

todoSchema.statics.todo_update_complete = function (todo_id, x) {
    return this.update({todo_id: todo_id}, {$set: x}).exec(); // Should return a Promise
}

todoSchema.statics.todo_update_for_wip_cncl = function (todo_id) {
    var d = new Date();
    d.setHours(d.getHours());
    return this.update({todo_id: todo_id}, {$set: {wip_ts: d, stop_ts: d}}).exec(); // Should return a Promise
}

todoSchema.statics.get_todo_details_by_type_status = function (query) {
    return this.find(query).count().exec(); // Should return a Promise
}

todoSchema.statics.get_total_call_made = function (query) {
    return this.find(query).count().exec(); // Should return a Promise
}


todoSchema.statics.get_all = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .sort({area: 1})
        .exec(); // Should return a Promise
}


todoSchema.statics.report_pending_vs_complted = function (res) {
    var x = this;
    x.count({
            $and: [
                {"type": "KYC"},
                {"status": "Pending"},
                {"old_record": "0"}
            ]
        },

        function (err, c) {
            var KYC_call_Pending = c;
            x.count({
                    $and: [
                        {"type": "KYC"},
                        {"status": "Completed"},
                        {"old_record": "0"}
                    ]
                },

                function (err, c) {
                    var KYC_call_Completed = c;
                    x.count({
                            $and: [
                                {"type": "GV Confirmation"},
                                {"status": "Pending"},
                                {"old_record": "0"}
                            ]
                        },

                        function (err, c) {
                            var GVConfirmation_Pending = c;
                            x.count({
                                    $and: [
                                        {"type": "GV Confirmation"},
                                        {"status": "Completed"},
                                        {"old_record": "0"}
                                    ]
                                },

                                function (err, c) {
                                    var GVConfirmation_Completed = c;
                                    x.count({
                                            $and: [
                                                {"type": "GV Reconfirmation"},
                                                {"status": "Pending"},
                                                {"old_record": "0"}
                                            ]
                                        },

                                        function (err, c) {
                                            var GV_Reconfirmation_Pending = c;
                                            x.count({
                                                    $and: [
                                                        {"type": "GV Reconfirmation"},
                                                        {"status": "Completed"},
                                                        {"old_record": "0"}
                                                    ]
                                                },

                                                function (err, c) {
                                                    var GV_Reconfirmation_Completed = c;
                                                    x.count({
                                                            $and: [
                                                                {"type": "Missed Call"},
                                                                {"status": "Pending"},
                                                                {"old_record": "0"}
                                                            ]
                                                        },

                                                        function (err, c) {
                                                            var Missed_Call_Pending = c;
                                                            x.count({
                                                                    $and: [
                                                                        {"type": "Missed Call"},
                                                                        {"status": "Completed"},
                                                                        {"old_record": "0"}
                                                                    ]
                                                                },

                                                                function (err, c) {
                                                                    var Missed_Call_Completed = c;
                                                                    var result = {
                                                                        KYC_call_Pending: KYC_call_Pending,
                                                                        KYC_call_Completed: KYC_call_Completed,
                                                                        GVConfirmation_Pending: GVConfirmation_Pending,
                                                                        GVConfirmation_Completed: GVConfirmation_Completed,
                                                                        GV_Reconfirmation_Pending: GV_Reconfirmation_Pending,
                                                                        GV_Reconfirmation_Completed: GV_Reconfirmation_Completed,
                                                                        Missed_Call_Pending: Missed_Call_Pending,
                                                                        Missed_Call_Completed: Missed_Call_Completed
                                                                    };
                                                                    res.send(result);
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });
                }
            );
        }
    );


}

todoSchema.statics.report_today_call = function (y, m, d) {
    y = y * 1;
    m = m * 1;
    d = d * 1;
    var dt = new Date();
    var pipeline = [
        {
            $match: {
                $and: [
                    {wip_ts: {$ne: ""}},
                    {wip_ts: {"$exists": true}}
                ]
            }
        },
        {
            $project: {
                _id: 0,
                Region: "$region",
                District: "$district",
                City: "$city",

                "RetailerName": "$extra_info.retailer_name",
                "Outlet": "$extra_info.outlet_name",
                "Address": "$extra_info.address",
                "RegisteredMobile": "$extra_info.retailer_registered_mobile",
                "MissCallNumber": "$extra_info.mobile_number",
                "Type": "$type",
                "Status": "$status",
                "UserName": "$wip_user.user_first_name",
                "Reason": "$call_status",
                "Comment": "$call_comment",
                "CallStartTime": "$start_ts",

                //            "Call Stop Time" : "$stop_ts",
                //            Area:             "$area",
                //            Locality:         "$locality",
                //            assignment_type:  "$assignment_type",
                //            "Email Id":"$wip_user.user_email_id",
                //            wip_ts:           '$wip_ts' ,


                year: {$year: '$wip_ts'},
                month: {$month: '$wip_ts'},
                day: {$dayOfMonth: '$wip_ts'}

            }
        },
        {
            $match: {
                $and: [
                    {year: y},
                    {month: m},
                    {day: d}
                ]
            }
        }
    ];

    return this.aggregate(pipeline).exec();
}


todoSchema.statics.get_mobile_no_date = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("id created_dt status -_id").sort({created_dt: -1})
        .exec(); // Should return a Promise
}
todoSchema.statics.premier_search_by_role = function (role) {
    var pipeline = [
        {
            $match: {
                $and: [
                    role,
                    {status: "Pending"},
                    {wip_ts: {$not: {$gt: new Date()}}},
                    {old_record: "0"},
                    {active: 1}

                ]
            }
        },
        {
            "$group": {
                "_id": {
                    "type": "$type",
                    "region": "$region",
                    "state": "$state",
                    "district": "$district",
                    "city": "$city",
                    "area": "$area",
                    "id": "$id",
                    "locality": "$locality",
                    "distributor_name": "$extra_info.distributor_name",
                    "distributor_contact_person1_mobile": "$extra_info.distributor_contact_person1_mobile",
                    "todo_for": "$todo_for",
                    "retailer_status": "$retailer_status",
                    "todo_id": "$todo_id"
                },
                count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                "type": "$_id.type",
                "region": "$_id.region",
                "state": "$_id.state",
                "district": "$_id.district",
                "city": "$_id.city",
                "area": "$_id.area",
                "locality": "$_id.locality",
                "distributor_name": "$_id.distributor_name",
                "distributor_contact_person1_mobile": "$_id.distributor_contact_person1_mobile",
                "id": "$_id.id",
                "todo_for": "$_id.todo_for",
                "retailer_status": "$_id.retailer_status",
                "todo_id": "$_id.todo_id",
                "count": "$count"

            }
        }

    ];
    return this.aggregate(pipeline).exec(); // Should return a Promise
}


//added by Mahiruddin Seikh on: 23/11/2015
todoSchema.statics.complete_motivation = function (q, data) {
    return this.update(
        q, data
        )
        .exec();
}


todoSchema.set('collection', 'todo');
module.exports = mongo.get_mongoose_connection().model('todo', todoSchema);
