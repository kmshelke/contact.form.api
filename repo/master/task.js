/**
 * Created by developer6 on 6/19/2015.
 */


var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var taskSchema = new Schema({
    task_id                : { type: String, required: true, unique: true},
    state                  : { type: String,  trim: true },
    retailer               : { type: String,  trim: true },
    retailer_code          : { type: String,  trim: true },
    retailer_name          : { type: String,  trim: true },
    city                   : { type: String,  trim: true },
    area                   : { type: String,  trim: true },
    user                   : { type: String,  trim: true },
    user_id                : { type: String,  trim: true },
    user_name              : { type: String,  trim: true },
    task                   : { type: String,  trim: true },
    description            : { type: String,  trim: true },
    assign_date            : { type: Date},
    status                 : { type: String,  trim: true },
    active                 : { type: Number },
    created_by             : { type: String,  trim: true },
    created_dt             : { type: Date},
    modified_by            : { type: String,  trim: true },
    modified_dt            : { type: Date}
},{ versionKey: false });

var include_field = "task_id state retailer retailer_code retailer_name city area user user_id user_name task description assign_date created_dt status active -_id";

taskSchema.statics.get_task_list = function (query,skip,limit) {

//    console.log(query);

    return this.find(query).skip(skip).limit(limit).select(include_field).exec();// Should return a Promise
}


taskSchema.set('collection', 'task');
module.exports = mongo.get_mongoose_connection().model('task', taskSchema);
