var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var sales_registrySchema = new Schema({
    sales_id         : { type: String, trim: true,required:true,unique:true },
    sales_date       : { type: Date },
    dist_code        : { type: String, trim: true },
    dist_name        :{ type: String, trim: true },
    retailer_phone_no: { type: String, trim: true },
    retailer_code    : { type: String, trim: true },
    retailer_name    : { type: String, trim: true },
    sales_mode       : { type: String, trim: true },
    reference_type   : { type: String, trim: true },
    reference_number :  { type: String, trim: true },
    coupon_code      : { type: String, trim: true },
    coupon_detail    : { type: String, trim: true },
    points_calculated: { type: String, trim: true },
    points_earned    : { type: Number },
    status           :  { type: String, trim: true },
    created_by       : { type: String, trim: true },
    created_dt       :  { type: Date },
    modified_by      : { type: String, trim: true },
    modified_dt      :  { type: Date }
},{ versionKey: false });


var include_field="sales_id dist_code dist_name retailer_phone_no retailer_code retailer_name sales_mode coupon_code status -_id"

//Get Sales registry Details
sales_registrySchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}

sales_registrySchema.statics.get_coupon_count = function (query) {
    return this.find(query).count().exec();// Should return a Promise
}

sales_registrySchema.statics.retailer_total_coupon_count = function (retailer_code) {

    var arr_match=[
        {"active" : 1},
        {status:{$in : ['Confirmed','Physically Collected']} }
    ];

    if(retailer_code){
        if(retailer_code!='' && retailer_code!=null){
            var orc={'retailer_code' : retailer_code}
            arr_match.push(orc);
        }
    }

//    console.log(arr_match);

    var pipeline = [
        {$match: { $and : arr_match } },
        {$group: { _id: {'retailer_code' : '$retailer_code','status': '$status'},
            total_count : {$sum : 1}
        }},
        {$project:
        {_id:0,  id: '$_id.retailer_code',
            total_count: '$total_count'}},
        {$sort: {total_count:-1}}

    ];

//    console.log(pipeline);

    return this.aggregate(pipeline).exec();// Should return a Promise

}

sales_registrySchema.statics.retailer_total_coupon_count2 = function (retailer_code) {

//    console.log(retailer_code);

    var arr_match=[
        {"active" : 1},
        {status:{$in : ['Confirmed','Physically Collected']} },
        {retailer_code : {$in: retailer_code }}
    ];

    var pipeline = [
        {$match: { $and : arr_match } },
        {$group: { _id: {'retailer_code' : '$retailer_code','status': '$status'},
            total_count : {$sum : 1}
        }},
        {$project:
        {_id:0,  id: '$_id.retailer_code',
            total_count: '$total_count'}},
        {$sort: {total_count:1}}

    ];

//    console.log(pipeline);

    return this.aggregate(pipeline).exec();// Should return a Promise

}

sales_registrySchema.statics.retailer_redeem_point = function (retailer_code) {

    var arr_match2=[
        {redeemed:{"$exists":true}},
        {redeemed: 'Y'}
    ];

    if(retailer_code){
        if(retailer_code!='' && retailer_code!=null){
            var orc={'retailer_code' : retailer_code};
            arr_match2.push(orc);
        }
    }

    var pipeline = [
        {$match: { $and : arr_match2 } },
        {$group: { _id: {'redeemed' : '$redeemed','retailer_code': '$retailer_code'},
            total_count : {$sum : 1}
        }},
        {$project:
        {_id:0,  id: '$_id.retailer_code',
            redeem: '$total_count'}},
        {$sort: {total_count:-1}}

    ];

//    console.log(pipeline);

    return this.aggregate(pipeline).exec();// Should return a Promise

}


sales_registrySchema.set('collection', 'sales_registry');
module.exports = mongo.get_mongoose_connection().model('sales_registry', sales_registrySchema);