/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var localitySchema = new Schema({
    state             : { type: String,  trim: true },
    city              : { type: String,  trim: true },
    area              : { type: String,  trim: true },
    locality          : { type: String,  trim: true,required: true, unique: true }

},{ versionKey: false }
 );

var include_field = "state city locality -_id ";


//Get locality Details
localitySchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("locality")
        .exec();// Should return a Promise
}

localitySchema.set('collection', 'locality');
module.exports = mongo.get_mongoose_connection().model('locality', localitySchema);