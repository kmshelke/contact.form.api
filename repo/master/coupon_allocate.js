/**
 * Created by Hassan on 6/5/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var coupons_allocationSchema = new Schema({
    "coupon_from" : {type: Number},
    "coupon_to" : {type: Number},
    "coupon_range" : { type: String, trim: true },
    "brand_code" : { type: String, trim: true },
    "coupon_brand" :{ type: String, trim: true },
    "coupon_distributor_code" : { type: String, trim: true },
    "coupon_distributor_name" : { type: String, trim: true },
    "status" :    { type: String, trim: true },
    "active" : {type: Number},
    "remarks" : { type: String, trim: true },
    "created_by" : { type: String, trim: true },
    "created_dt" :  { type: Date },
    "modified_by" : { type: String, trim: true },
    "modified_dt" :  { type: Date }
},{ versionKey: false });

var include_fields ="coupon_from coupon_to coupon_range brand_code coupon_brand coupon_distributor_code coupon_distributor_name status created_dt -_id";
// static methods


//Get all allocated coupons Details
coupons_allocationSchema.statics.all_allocated_coupon_range = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit).select(include_fields).exec();// Should return a Promise
}


coupons_allocationSchema.set('collection', 'coupons_allocation');
module.exports = mongo.get_mongoose_connection().model('coupons_allocation', coupons_allocationSchema);
