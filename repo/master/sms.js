/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var smsSchema = new Schema({
    sms_id                 : { type: Number, required: true, unique: true},
    sms_from_mobile        : { type: String,  trim: true },
    sms_text               : { type: String,  trim: true },
    sms_timestamp          : { type: Date},
    sms_operator           : { type: String,  trim: true },
    sms_circle             : { type: String,  trim: true },
    sms_status             : { type: Number},
    sms_processed          : { type: String,  trim: true },
    sms_processed_time     : { type: Date},
    sms_processed_error    : { type: String,  trim: true },
    sms_from_retailer_code : { type: String,  trim: true },
    sms_from_retailer_name : { type: String,  trim: true },
    created_by             : { type: String,  trim: true },
    created_dt             : { type: Date},
    modified_by            : { type: String,  trim: true },
    modified_dt            : { type: Date}
},{ versionKey: false });

var include_field = "sms_id sms_from_mobile sms_text sms_timestamp sms_operator sms_circle sms_processed sms_processed_time sms_from_retailer_code sms_from_retailer_name sms_processed_error -_id";

//Update SMS Detail
smsSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

//Get SMS Details
smsSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}


smsSchema.set('collection', 'sms');
module.exports = mongo.get_mongoose_connection().model('sms', smsSchema);