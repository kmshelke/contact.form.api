/**
 * Created by Hassan Ahamed on 6/23/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var assigned_rosterSchema = new Schema({
    "assignment_date"           : {type: Date},
    "assignment_type"           : { type: String, trim: true },
    "user_id"                   : { type: String, trim: true },
    "name"                      : { type: String, trim: true },
    "role"                      :{ type: String, trim: true },
    "region"                    : { type: String, trim: true },
    "area"                    : { type: String, trim: true },
    "locality"                    : { type: String, trim: true },
    "active"                    : {type: Number},
    "created_by"                : { type: String, trim: true },
    "created_dt"                :  { type: Date },
    "modified_by"               : { type: String, trim: true },
    "modified_dt"               :  { type: Date }
},{ versionKey: false });

var include_fields ="assignment_date assignment_type user_id name role region area locality -_id";
// static methods


//Get all Default Roster
assigned_rosterSchema.statics.get_assnmt_types_for_today = function(user_id,dt){

    console.log(dt);

    var pipeline = [
        {$match: {$and :
            [{active:1},{user_id:user_id}]
        }},
        {$project:{ assignment_date: '$assignment_date' ,
            assignment_type:  "$assignment_type",
            user_id:          "$user_id",
            name :            "$name",
            role :            "$role",
            region :          "$region",
            state :           "$state",
            district :        "$district",
            city :            "$city",
            area:             "$area",
            locality:         "$locality",
            active :          "$active",
            year:             {$year: '$assignment_date'},
            month:            {$month: '$assignment_date'},
            day:              {$dayOfMonth: '$assignment_date'}

        }}
        ,
        {$match: {$and :
            [
                {year: dt.year},
                {month: dt.month},
                {day: dt.day}
            ]
        }}
    ];
    return this.aggregate (pipeline).exec();// Should return a Promise
}

assigned_rosterSchema.set('collection', 'assigned_roster');
module.exports = mongo.get_mongoose_connection().model('assigned_roster', assigned_rosterSchema);


