/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var usergroupSchema = new Schema({
        group_id           : { type: Number,  required: true},
        group_name         : { type: String,  trim: true,unique: true },
        group_desc         : { type: String,  trim: true },
        ref_code           : { type: Number,  trim: true },
        data_source        : { type: String,  trim: true },
        rights: [
        {
            menu_id        : { type: Number},
            menu_name      : { type: String,  trim: true },
            right_view     : { type: Number},
            right_edit     : { type: Number},
            right_add      : { type: Number},
            right_delete   : { type: Number},
            right_print    : { type: Number},
            right_upload   : { type: Number},
            right_download : { type: Number}

        }
    ],
    status                  : { type: Number},
    created_by              : { type: String,  trim: true },
    created_dt              : { type: Date},
    modified_by             : { type: String,  trim: true },
    modified_dt             : { type: Date}
},{ versionKey: false }
 );


var include_field = "group_id group_name group_desc rights -_id";

//Update Usergroup Detail
usergroupSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

//Get Usergroup Details
usergroupSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}


//Get Usergroup Id and Name
usergroupSchema.statics.get_group_id_name = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("group_id group_name -_id")
        .exec();// Should return a Promise
}

//Get Usergroup Rights
usergroupSchema.statics.get_group_rights = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("rights group_name group_desc  -_id")
        .exec();// Should return a Promise
}


//Get Usergroup last record
usergroupSchema.statics.get_group_last_record = function (query,skip,limit) {
    return this.find(query).sort({_id:-1}).limit(1)
        .select("group_id -_id")
        .exec();// Should return a Promise
}
usergroupSchema.set('collection', 'usergroup');
module.exports = mongo.get_mongoose_connection().model('usergroup', usergroupSchema);