/**
 * Created by Hassan Ahamed on 6/23/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var default_rosterSchema = new Schema({
    "assignment_date"           : {type: Date},
    "assignment_type"           : { type: String, trim: true },
    "user_id"                   : { type: String, trim: true },
    "name"                      : { type: String, trim: true },
    "role"                      : { type: String, trim: true },
    "region"                    : { type: String, trim: true },
    "state"                     : { type: String, trim: true },
    "district"                  : { type: String, trim: true },
    "city"                      : { type: String, trim: true },
    "area"                      : { type: String, trim: true },
    "locality"                  : { type: String, trim: true },
    "active"                    : {type: Number},
    "created_by"                : { type: String, trim: true },
    "created_dt"                : { type: Date },
    "deactivate_date"           : { type: Date },
    "modified_by"               : { type: String, trim: true },
    "modified_dt"               : { type: Date }
},{ versionKey: false });

var include_fields ="assignment_date assignment_type user_id name role region area locality -_id";
// static methods


//Get all Default Roster
default_rosterSchema.statics.get_all_default_roster = function(){
    var pipeline = [
        {$match: {$and :
            [
                {active:1}
            ]
        }},
        {$group : { _id: {'user_id': '$user_id', 'name': '$name' ,'role' : '$role', 'region' : '$region','area' : '$area','locality' : '$locality','city' : '$city','district' : '$district'}}},
        {$project: {
            _id: 0,
            "user_id"  : "$_id.user_id",
            "role"     : "$_id.role",
            "region"   : "$_id.region",
            "area"     : "$_id.area",
            "locality" : "$_id.locality",
            "name"     : "$_id.name",
            "city"     : "$_id.city",
            "district"     : "$_id.district"
        }}
    ];
    return this.aggregate (pipeline).sort({region:1}).exec();// Should return a Promise
}

default_rosterSchema.statics.get_selected_date_roster = function(){
    var pipeline = [
        {$match: {$and :
            [
                {active:1}
            ]
        }},
        {$group : { _id: {'user_id': '$user_id', 'name': '$name' ,'role' : '$role', 'region' : '$region'}}},
        {$project: {
            _id: 0,
            "user_id"  : "$_id.user_id",
            "role"     : "$_id.role",
            "region"   : "$_id.region",
            "name"     : "$_id.name"
        }}
    ];
    return this.aggregate(pipeline).exec();// Should return a Promise
}

default_rosterSchema.statics.get_default_roster_by_usr_id = function (query) {
    return this.find(query).select("user_id active role region state district city area locality -_id").exec();// Should return a Promise
}


default_rosterSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data,{multi: true}
    )
        .exec();
}


default_rosterSchema.statics.get_telecallers_count = function(){
    var pipeline = [
        {$group: { _id: {'name' : "$name" },
            total_count : {$sum : 1}

        }},
        {$project:
        {_id:0,  name: '$_id.name', total_count: '$total_count'}}
    ];
    return this.aggregate(pipeline).exec();// Should return a Promise
}

default_rosterSchema.set('collection', 'default_roster');
module.exports = mongo.get_mongoose_connection().model('default_roster', default_rosterSchema);
