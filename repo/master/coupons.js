/**
 * Created by Hassan Ahamed on 6/01/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var couponsSchema = new Schema({
    "coupon_serial_no" : {type: Number},
    "coupon_code" : { type: String, trim: true },
    "brand_code" : { type: String, trim: true },
    "coupon_brand" :{ type: String, trim: true },
    "coupon_unit" : { type: String, trim: true },
    "coupon_quantity" : {type: Number},
    "coupon_distributor_code" : { type: String, trim: true },
    "coupon_distributor_name" : { type: String, trim: true },
    "coupon_expiry" :  { type: Date },
    "coupon_status" : {type: Number},
    "active" : {type: Number},
    "status" :    { type: String, trim: true },
    "coupon_collected" : { type: String, trim: true },
    "coupon_collected_by" : { type: String, trim: true },
    "coupon_collected_date" :  { type: Date },
    "coupon_remarks" : { type: String, trim: true },
    "shipping_dt" : { type: Date },
    "courier_service_provider" : { type: String, trim: true },
    "courier_track_no" : { type: String, trim: true },
    "courier_track_link" : { type: String, trim: true },
    "courier_cost" : { type: String, trim: true },
    "dispatch_status" : { type: String, trim: true },
    "dispatch_dt" : { type: Date },
    "created_by" : { type: String, trim: true },
    "created_dt" :  { type: Date },
    "modified_by" : { type: String, trim: true },
    "modified_dt" :  { type: Date }
},{ versionKey: false });

var include_fields ="coupon_serial_no coupon_code  coupon_brand coupon_quantity status brand_code -_id";
// static methods

couponsSchema.statics.get_brand_coupon_max_val= function(brand_code){

    var p = {"brand_code":brand_code};//Brand Code Should Be Unique
    console.log(p);
    return this.find(p).select(include_fields).sort({"coupon_serial_no":-1}).limit(1).exec();// Should return a Promise

}

//Update Coupons
couponsSchema.statics.allocate_coupon = function (q,data) {
    return this.update(q,data,{multi: true}).exec();
}

//Get coupons Details
couponsSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("coupon_code  coupon_brand coupon_distributor_name coupon_distributor_code coupon_collected coupon_collected_by status coupon_serial_no brand_code dispatch_status allocated_dt coupon_distributor_mobile_no courier_track_no courier_track_link -_id").sort({"dispatch_status":1})
        .exec();// Should return a Promise
}

//Get coupon dist code,name and status
couponsSchema.statics.get_coupon_status = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("coupon_distributor_code  coupon_distributor_name status -_id")
        .exec();// Should return a Promise
}

//Update coupon Status
couponsSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

couponsSchema.statics.get_generated_coupon_count = function (query) {
    return this.find(query).count().exec();// Should return a Promise
}

couponsSchema.statics.get_coupons_by_coupon_code = function (query) {
    return this.find(query)
        .select(include_fields)
        .exec();// Should return a Promise
}


//Get coupons Details
couponsSchema.statics.get_courier = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("coupon_code shipping_dt courier_service_provider courier_track_no courier_track_link courier_cost dispatch_status dispatch_dt -_id")
        .exec();// Should return a Promise
}
couponsSchema.set('collection', 'coupons');
module.exports = mongo.get_mongoose_connection().model('coupons', couponsSchema);
