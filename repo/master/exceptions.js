var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var exceptionsSchema = new Schema({
    exception_id     : { type: String, trim: true,required:true,unique:true },
    exception_date   : { type: Date },
    dist_code        : { type: String, trim: true },
    dist_name        : { type: String, trim: true },
    retailer_phone_no: { type: String, trim: true },
    retailer_code    : { type: String, trim: true },
    retailer_name    : { type: String, trim: true },
    sales_mode       : { type: String, trim: true },
    reference_type   : { type: String, trim: true },
    reference_number : { type: String, trim: true },
    coupon_code      : { type: String, trim: true },
    status           : { type: String, trim: true },
    created_by       : { type: String, trim: true },
    created_dt       :  { type: Date },
    modified_by      : { type: String, trim: true },
    modified_dt      :  { type: Date }
},{ versionKey: false });

var include_field="exception_id dist_code dist_name retailer_code retailer_name sales_mode coupon_code status retailer_phone_no -_id"

//Get Exception Details
exceptionsSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}


//Update Distributor Detail
exceptionsSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

//Get Exception mobile no and date
exceptionsSchema.statics.get_mobile_no_date = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("exception_date retailer_phone_no -_id")
        .exec();// Should return a Promise
}

exceptionsSchema.set('collection', 'exceptions');
module.exports = mongo.get_mongoose_connection().model('exceptions', exceptionsSchema);