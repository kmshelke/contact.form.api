/**
 * Created by Hassan Ahamed on 5/27/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');
var shortid = require('shortid');

var keep_trail = function (business_id, business_id_name, collection_name, db_name, field_name, old_data, new_data, logger_msg) {
    var a = new audit();
    a.business_id = business_id;
    a.business_id_name = business_id_name;
    a.collection_name = collection_name;
    a.db_name = db_name;
    a.field_name = field_name;
    a.old_data = old_data;
    a.new_data = new_data;
    a.keep_trail();
    logger.info(logger_msg);
}


var locationSchema = new Schema({
    code: { type: String, trim: true, unique: true},
    type: { type: String, trim: true },
    parent: { type: String, trim: true},
    value: { type: String, trim: true },
    active: { type: String, trim: true },
    created_by: { type: String, trim: true },
    created_dt: { type: Date }
}, { versionKey: false });

//Get Brands Details
locationSchema.statics.get_location = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("type parent value -_id")
        .sort({value: 1})
        .exec();// Should return a Promise
}

locationSchema.statics.get_district_city_location = function (query) {
    return this.find(query)
        .select("type parent value -_id")
        .sort({value: 1})
        .exec();// Should return a Promise
}

locationSchema.statics.get_location_by_details = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("type parent value active code  -_id")
        .sort({value: 1})
        .exec();// Should return a Promise
}


locationSchema.set('collection', 'location')

module.exports = mongo.get_mongoose_connection().model('brands', locationSchema);