/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var distributorSchema = new Schema({
    distributor_code: { type: String, trim: true, required: true, unique: true},
    distributor_name: { type: String, trim: true },
    distributor_region: { type: String, trim: true },
    distributor_state: { type: String, trim: true },
    distributor_city: { type: String, trim: true },
    distributor_address: { type: String, trim: true },
    distributor_district: { type: String, trim: true },
    distributor_pin: { type: String, trim: true },
    distributor_area: { type: String, trim: true },
    distributor_locality: { type: String, trim: true },
    distributor_contact_person1: { type: String, trim: true },
    distributor_contact_person1_mobile: { type: String, trim: true },
    distributor_contact_person2: { type: String, trim: true },
    distributor_contact_person2_mobile: { type: String, trim: true },
    distributor_regional_mgr_name: { type: String, trim: true },
    distributor_regional_mgr_mobile: { type: String, trim: true },
    distributor_landline_no: { type: String, trim: true },
    distributor_std_code: { type: String, trim: true },
    landmark: { type: String, trim: true },
    comment: { type: String, trim: true },
    status: { type: String, trim: true },
    distributor_brand: [
        {
            brand: { type: String, trim: true },
            name: { type: String, trim: true },
            type: { type: String, trim: true }
        }
    ],
    kyc_status: { type: String, trim: true },
    email : { type: String, trim: true },

    distributor_informed: { type: String, trim: true },

    distributor_comments: [
        {
            call_type: { type: String, trim: true },
            retailer_code: { type: String, trim: true },
            todo_id: { type: String, trim: true },
            call_status: { type: String, trim: true },
            created_by: { type: String, trim: true },
            created_dt: { type: Date},
            comment: { type: String, trim: true },
            complaint: { type: String, trim: true }
        }
    ],

    created_by: { type: String, trim: true },
    created_dt: { type: Date},
    modified_by: { type: String, trim: true },
    modified_dt: { type: Date}
}, { versionKey: false });

var include_field = "distributor_code distributor_name email distributor_comments distributor_region distributor_locality distributor_city distributor_pin distributor_address distributor_district distributor_contact_person1 distributor_contact_person1_mobile distributor_contact_person2 distributor_contact_person2_mobile distributor_regional_mgr_name distributor_regional_mgr_mobile distributor_landline_no distributor_std_code landmark comment kyc_status distributor_state distributor_brand distributor_area -_id";
var include_dist = "distributor_code distributor_comments email distributor_name -_id"

//Update Distributor Detail
distributorSchema.statics.update_details = function (q, data) {
    return this.update(
        q, data
    )
        .exec();
}

//Get Distributor Details
distributorSchema.statics.get_list = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}

//Get Distributor code and name
distributorSchema.statics.get_distributor_code_name = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("distributor_code distributor_name -_id").sort({distributor_name: 1})
        .exec();// Should return a Promise
}

//Get Distributor brand
distributorSchema.statics.get_distributor_brand = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("distributor_brand -_id")
        .exec();// Should return a Promise
}

//check existence of mobile no
distributorSchema.statics.get_mob_no = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("distributor_code distributor_name -_id")
        .exec();// Should return a Promise
}



distributorSchema.set('collection', 'distributor');
module.exports = mongo.get_mongoose_connection().model('distributor', distributorSchema);