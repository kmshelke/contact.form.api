
/**
 * Created by Hassan Ahamed on 5/27/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var brandsSchema = new Schema({
    brand         :  { type: String, trim: true },
    short_code    :  { type: String, trim: true,unique:true, },
    no_of_coupons : {type: Number},
    active        : {type: Number}
},{ versionKey: false });

var include_fields ="brand short_code  no_of_coupons -_id";
// static methods



brandsSchema.statics.get_brands_data= function(brand_code){

    var p = {"active":1,"short_code":brand_code};//Brand Code Should Be Unique
    console.log(p);
    return this.find(p).select(include_fields).exec();// Should return a Promise

}


//Update brands Detail
brandsSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

//Get Brands Details
brandsSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_fields)
        .exec();// Should return a Promise
}


brandsSchema.set('collection', 'brands')

module.exports = mongo.get_mongoose_connection().model('brands', brandsSchema);