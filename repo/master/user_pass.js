/**
 * Created by Developer9 on 6/15/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var userpassSchema = new Schema({
    user_id          :  { type: String,  trim: true},
    user_email_id    : { type: String,  trim: true },
    status           : { type: Number},
    created_dt       : {type:Date},
    expire_dt      : { type: Date},
    token_id      : {type:String}
},{ versionKey: false });

//var include_field = "user_id user_first_name user_middle_name user_last_name user_designation user_password user_email_id user_group_id -_id";

userpassSchema.statics.update_token = function (q,data) {
    return this.update(q,data).exec();
}
userpassSchema.statics.token_match = function (q) {
    return this.find(q).exec();
}



userpassSchema.set('collection', 'user_pass');
module.exports = mongo.get_mongoose_connection().model('user_pass', userpassSchema);