/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');
var autoIncrement = require("mongodb-autoincrement");

var retailerSchema = new Schema({
        retailer_code: {type: String, trim: true, required: true, unique: true},
        retailer_serial_no: {type: Number},
        retailer_name: {type: String, trim: true},
        retailer_dist_code: {type: String, trim: true},
        retailer_dist_name: {type: String, trim: true},
        retailer_dist_number: {type: String, trim: true},
        retailer_region: {type: String, trim: true},
        retailer_state: {type: String, trim: true},
        locality: {type: String, trim: true},
        tl_role: {type: String, trim: true},
        tl_todo_id: {type: String, trim: true},
        retailer_address: {type: String, trim: true},
        retailer_district: {type: String, trim: true},
        retailer_pin: {type: String, trim: true},
        retailer_city: {type: String, trim: true},
        retailer_area: {type: String, trim: true},
        std_code: {type: String, trim: true},

        gv_retailer_status: {type: String, trim: true},

        asm_name: {type: String, trim: true},
        asm_number: {type: String, trim: true},
        asm_email: {type: String, trim: true},

        otp_validate_status: {type: String, trim: true},
        eligibility: {type: String, trim: true},

        extention_1: {type: String, trim: true},
        extention_2: {type: String, trim: true},

        birthday: {type: String, trim: true},
        emailid: {type: String, trim: true},

        call_role: {type: String, trim: true},

        business_anniversary: {type: String, trim: true},
        no_of_children: {type: String, trim: true},
        marriage_anniversary: {type: String, trim: true},
        age_of_children: {type: String, trim: true},

        preferred_language: {type: String, trim: true},
        mobile_os: {type: String, trim: true},

        landmark: {type: String, trim: true},
        retailer_contacts: [
            {
                contact_sl_no: {type: Number},
                contact_uid: {type: String, trim: true},
                contact_mapped_retailer_code: {type: String, trim: true},
                contact_user_type: {type: String, trim: true},
                contact_f_name: {type: String, trim: true},
                contact_l_name: {type: String, trim: true},
                contact_name: {type: String, trim: true},
                contact_designation: {type: String, trim: true},
                contact_mobile_1: {type: String, trim: true},
                contact_mobile_2: {type: String, trim: true},
                contact_email_1: {type: String, trim: true},
                contact_email_2: {type: String, trim: true},
                contact_process_uid: {type: String, trim: true},
                contact_retailer_address: {type: String, trim: true},
                contact_retailer_region: {type: String, trim: true},
                contact_retailer_state: {type: String, trim: true},
                contact_retailer_district: {type: String, trim: true},
                contact_retailer_city: {type: String, trim: true},
                contact_retailer_area: {type: String, trim: true},
                contact_locality: {type: String, trim: true},
                contact_retailer_pin: {type: String, trim: true},
                contact_landmark: {type: String, trim: true},

                wedding_anniversary: {type: String, trim: true},
                contact_status: {type: Number}
            }
        ],
        retailer_rewards_delivery_addr: {type: String, trim: true},
        retailer_registered_mobile: {type: String, trim: true},
        details: [
            {
                type: {type: String, trim: true},
                doc_no: {type: String, trim: true},
                doc_dt: {type: String, trim: true},
                img_url: {type: String, trim: true}
            }
        ],
        retailer_monthly_turnover: {type: String, trim: true},
        weeklyoff: {type: String, trim: true},
        outlet_name: {type: String, trim: true},
        appointment_time: {type: String, trim: true},
        status: {type: String, trim: true},
        kyc_status: {type: String, trim: true},
        interest_status: {type: String, trim: true},
        final_data_status: {type: String, trim: true},
        data_source: {type: String, trim: true},
        kyc_call_status: {type: String, trim: true},
        gv_status: {type: String, trim: true},
        kyc_reverification_status: {type: String, trim: true},
        created_by: {type: String, trim: true},
        created_dt: {type: Date},
        modified_by: {type: String, trim: true},
        modified_dt: {type: Date},
        mc_info: {},
        other_info: {},
        motivation_done: {type: String, trim: true},
        retailer_gift: [],
        gift_comment: [],
        retailer_comments: [
            {
                call_type: {type: String, trim: true},
                retailer_code: {type: String, trim: true},
                todo_id: {type: String, trim: true},
                call_status: {type: String, trim: true},
                created_by: {type: String, trim: true},
                created_dt: {type: Date},
                comment: {type: String, trim: true},
                complaint: {type: String, trim: true}
            }
        ]
    }, {versionKey: false}
);

var include_field = "retailer_code retailer_name gv_retailer_status birthday emailid preferred_language mobile_os business_anniversary no_of_children marriage_anniversary age_of_children otherinfo std_code landmark retailer_dist_code retailer_dist_name retailer_dist_number asm_name asm_number asm_email otp_validate_status eligibility extention_1 extention_2 appointment_time outlet_name retailer_dist_code retailer_region retailer_address retailer_district retailer_pin retailer_rewards_delivery_addr retailer_registered_mobile kyc_status retailer_city retailer_monthly_turnover weeklyoff retailer_state locality details kyc_call_status gv_status kyc_reverification_status retailer_area interest_status retailer_contacts mc_info other_info retailer_comments -_id retailer_gift gift_comment retailer_family_code";
var include_field1 = "retailer_code retailer_name gv_retailer_status birthday emailid preferred_language mobile_os business_anniversary no_of_children marriage_anniversary age_of_children outlet_name retailer_region retailer_dist_code retailer_dist_name retailer_dist_number asm_name asm_number asm_email otp_validate_status eligibility extention_1 extention_2 retailer_state retailer_address retailer_district retailer_pin retailer_registered_mobile retailer_city locality retailer_area  -_id retailer_gift gift_comment ";
var include_field2 = "retailer_gift";

//Get Retailer List By State
retailerSchema.statics.get_retailer_by_state = function (query) {
    return this.find(query).select("retailer_code retailer_name -_id").exec();// Should return a Promise
}


//Update Retailer Detail
retailerSchema.statics.update_details = function (q, data) {
    return this.update(
        q, data
    )
        .exec();
}

//Get Retailer Details
retailerSchema.statics.get_list = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}



//Get Last Added Retailer Sl No.
//Get Retailer Details
retailerSchema.statics.get_last_sl_no = function (query, proj, sor, limit) {
    return this.find(query).sort(sor).limit(limit)
        .select(proj)
        .exec();// Should return a Promise
}


//Get Retailer Details
retailerSchema.statics.get_retailer_list_data = function (query, skip, limit) {
    //return this.find(query).skip(skip).limit(limit).select("retailer_code outlet_name retailer_name retailer_registered_mobile -_id").exec();// Should return a Promise
    return this.find(query).skip(skip).limit(limit).select(include_field).exec();// Should return a Promise
}


//Get Retailer Contacts Depending on Retailer id
retailerSchema.statics.get_retailer_contacts = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("retailer_contacts -_id")
        .exec();// Should return a Promise
}

//Get Retailer Contacts Depending on Retailer id
retailerSchema.statics.retailer_gift_list = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("retailer_code retailer_name outlet_name retailer_registered_mobile retailer_gift gift_comment -_id")
        .exec();// Should return a Promise
}


//Get Retailer code and name and registered mobile Depending on Retailer city
retailerSchema.statics.get_retailer_name_by_id = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("retailer_code retailer_name outlet_name retailer_registered_mobile -_id")
        .exec();// Should return a Promise
}

//check existence of mobile no
retailerSchema.statics.get_mob_no = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select("retailer_code outlet_name retailer_registered_mobile retailer_contacts -_id")
        .exec();// Should return a Promise
}


// Get full retailer details (Dipanjan)

retailerSchema.statics.get_retailer_full_details = function () {
    return this.find()
        .select("retailer_code retailer_name details -_id")
        .exec();// Should return a Promise
}

retailerSchema.statics.report_new_enrollments = function () {
    return this.find({
            $or: [
                {kyc_call_status: "Completed"},
                {"created_by": {$ne: "ETLsystem"}},
                {"modified_dt": {$exists: true}}
            ]
        }
    )
        .select("retailer_region retailer_district retailer_city outlet_name retailer_name retailer_registered_mobile -_id")
        .exec();// Should return a Promise
}

retailerSchema.statics.report_not_interested_shops = function () {
    return this.find({interest_status: "Not Interested"})
        .select("retailer_region retailer_district retailer_city outlet_name retailer_name retailer_registered_mobile -_id")
        .exec();// Should return a Promise
}

// End


//autoIncrement.setDefaults({
//    collection: retailerSchema,     // collection name for counters, default: counters
//    field: retailerSchema.retailer_code,               // auto increment field name, default: _id
//    step: 1             // auto increment step
//});


//Get Retailer Details For Motivation Call
retailerSchema.statics.get_retailer_for_motivation = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field1)
        .exec();// Should return a Promise
}

//**************************Retailer Search For Call Assignement Start***********************************************
retailerSchema.statics.get_retailer_for_assign = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field1)
        .exec();// Should return a Promise
}

//**************************Retailer Search For Tele Call Entry Start***********************************************
retailerSchema.statics.get_retailers_for_approve = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field1)
        .exec();// Should return a Promise
}

//**************************Retailer Search For Call Assignement Start***********************************************
retailerSchema.statics.get_retailer_call_done = function (query) {
    return this.find(query)
        .select("retailer_code -_id")
        .exec();// Should return a Promise
}

//**************************Retailer Search For Call Assignement End************************************************


//Get Retailer Details For Motivation Call
retailerSchema.statics.get_retailer_coupon_count_for_motivation = function (query, skip, limit) {
    return this.find(query)
        .select(include_field1)
        .exec();// Should return a Promise
}

// Get Retailer Details For Physical Coupon Submission

retailerSchema.statics.get_retailer_for_physical_coupon_submission = function (query, skip, limit) {
    return this.find(query).skip(skip).limit(limit)
        .sort({retailer_area: 1})
        .select(include_field1)
        .exec();// Should return a Promise
}

retailerSchema.statics.get_retailer_data_by_mob = function (query) {
    return this.find(query)
        .select(include_field1)
        .exec();// Should return a Promise
}

retailerSchema.statics.get_retailer_data_by_reg_mob_no = function (query) {
    return this.find(query)
        .select(include_field)
        .exec();// Should return a Promise
}

retailerSchema.statics.get_gift_data = function (query) {
    return this.find(query)
        .select(include_field2)
        .exec();// Should return a Promise
}

retailerSchema.set('collection', 'retailer');
module.exports = mongo.get_mongoose_connection().model('retailer', retailerSchema);