/**
 * Created by Hassan Ahamed on 2/11/2016.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../../db_connect/mongoose.js');


var call_typeSchema = new Schema({
    code            :  { type: String, trim: true,unique:true},
    name            :  { type: String, trim: true },
    defination      :  { type: String, trim: true },
    active          :  {type: Number},
    created_by      :  { type: String, trim: true },
    created_dt      :  { type: Date },
    modified_by     :  { type: String, trim: true },
    modified_dt     :  { type: Date },
    basic_type      :  { type: String, trim: true }
},{ versionKey: false });

var include_fields ="code name  defination -_id";
// static methods



call_typeSchema.statics.get_call_type_data= function(q){
    return this.find(q).select(include_fields).exec();// Should return a Promise
}

//Update Call Type Detail
call_typeSchema.statics.update_call_type_details = function (q,data) {
    return this.update(q,data).exec();
}


call_typeSchema.set('collection', 'call_type')

module.exports = mongo.get_mongoose_connection().model('call_type', call_typeSchema);