/**
 * Created by Koninika on 01/06/2015.
 */

var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID

var mongo = require('../../db_connect/mongoose.js');

var communicationSchema = new Schema({
    comm_id         : { type: String,  trim: true ,required: true, unique: true},
    comm_date       : { type: Date},
    call_reference  : { type: String,  trim: true },
    start_call      : { type: String,  trim: true },
    end_call        : { type: String,  trim: true },
    call_to_number  : { type: String,  trim: true },
    call_entity_type: { type: String,  trim: true },
    call_entity_code: { type: String,  trim: true },
    call_notes      : { type: String,  trim: true },
    call_purpose    : { type: String,  trim: true },
    action_points   : { type: String,  trim: true },
    action_status   : { type: String,  trim: true },
    called_by       : { type: String,  trim: true },
    created_by      : { type: String,  trim: true },
    created_dt      : { type: Date},
    modified_by     : { type: String,  trim: true },
    modified_dt     : { type: Date}
},{ versionKey: false });

var include_field = "comm_id comm_date call_reference start_call end_call call_to_number call_entity_type call_entity_code call_notes call_purpose action_points action_status called_by -_id";

//Update Communication Detail
communicationSchema.statics.update_details = function (q,data) {
    return this.update(
        q,data
    )
        .exec();
}

//Get Communication Details
communicationSchema.statics.get_list = function (query,skip,limit) {
    return this.find(query).skip(skip).limit(limit)
        .select(include_field)
        .exec();// Should return a Promise
}


communicationSchema.set('collection', 'communications');
module.exports = mongo.get_mongoose_connection().model('communications', communicationSchema);